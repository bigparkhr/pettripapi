package com.bg.pettripapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Trip {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "petId")
    private Pet pet;

    @Column(nullable = false)
    private Boolean Attendance;

    @Column(nullable = false)
    private String TripDate;

    @Column(nullable = false)
    private Integer PetNumber;
}
