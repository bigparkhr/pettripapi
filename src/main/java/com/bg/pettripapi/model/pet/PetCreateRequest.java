package com.bg.pettripapi.model.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private Long id;
    private String name;
    private String phoneNumber;

}
