package com.bg.pettripapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetResponse {
    private Long id;
    private String name;
    private String phoneNumber;
}
