package com.bg.pettripapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetChangeRequest {
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;
}
