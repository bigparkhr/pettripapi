package com.bg.pettripapi.model.trip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripCreateRequest {
    private Boolean Attendance;
    private String TripDate;
    private Integer PetNumber;
}
