package com.bg.pettripapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetItem {
    private Long id;
    private String name;
}
