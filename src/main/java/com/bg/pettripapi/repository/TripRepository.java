package com.bg.pettripapi.repository;

import com.bg.pettripapi.entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TripRepository extends JpaRepository<Trip, Long> {
}
