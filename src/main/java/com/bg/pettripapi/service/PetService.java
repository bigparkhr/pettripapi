package com.bg.pettripapi.service;

import com.bg.pettripapi.entity.Pet;
import com.bg.pettripapi.model.PetChangeRequest;
import com.bg.pettripapi.model.PetItem;
import com.bg.pettripapi.model.PetResponse;
import com.bg.pettripapi.model.pet.PetCreateRequest;
import com.bg.pettripapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public Pet getData(long id) {
        return petRepository.findById(id).orElseThrow();
    }

    public void setPet(PetCreateRequest request) {
        Pet addData = new Pet();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());

        petRepository.save(addData);
    }

    public List<PetItem> getpets() {
        List<Pet> originList = petRepository.findAll();

        List<PetItem> result = new LinkedList<>();

        for (Pet pet : originList) {
            PetItem addItem = new PetItem();
            addItem.setId(pet.getId());
            addItem.setName(pet.getName());

            result.add(addItem);
        }
        return result;
    }

    public PetResponse getPet(long id) {
        Pet originData = petRepository.findById(id).orElseThrow();
        PetResponse response = new PetResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;

    }

    public void putPet(long id, PetChangeRequest request) {
        Pet originData = petRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());

        petRepository.save(originData);
    }
}
