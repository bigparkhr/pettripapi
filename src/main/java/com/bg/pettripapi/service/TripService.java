package com.bg.pettripapi.service;

import com.bg.pettripapi.entity.Pet;
import com.bg.pettripapi.entity.Trip;
import com.bg.pettripapi.model.trip.TripCreateRequest;
import com.bg.pettripapi.repository.TripRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TripService {
    private final TripRepository tripRepository;

    public void setTrip(Pet pet, TripCreateRequest request) {
        Trip addData = new Trip();
        addData.setPet(pet);
        addData.setAttendance(request.getAttendance());
        addData.setTripDate(request.getTripDate());
        addData.setPetNumber(request.getPetNumber());

        tripRepository.save(addData);
    }
}
