package com.bg.pettripapi.controller;

import com.bg.pettripapi.entity.Pet;
import com.bg.pettripapi.model.trip.TripCreateRequest;
import com.bg.pettripapi.service.PetService;
import com.bg.pettripapi.service.TripService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trip")
public class TripController {
    private final PetService petService;
    private final TripService tripService;

    @PostMapping("/new/pet-id/{petId}")
    public String setTrip(@PathVariable long petId, @RequestBody TripCreateRequest request) {
        Pet pet = petService.getData(petId);
        tripService.setTrip(pet, request);

        return "OK";
    }
}
