package com.bg.pettripapi.controller;

import com.bg.pettripapi.model.PetChangeRequest;
import com.bg.pettripapi.model.PetItem;
import com.bg.pettripapi.model.PetResponse;
import com.bg.pettripapi.model.pet.PetCreateRequest;
import com.bg.pettripapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final PetService petService;

    @PostMapping("/join")
    public String setPet(@RequestBody PetCreateRequest request) {
        petService.setPet(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PetItem> getPet() { return petService.getpets(); }

    @GetMapping("/detail/{id}")
    public PetResponse getPet(@PathVariable long id) {
        return petService.getPet(id);
    }

    @PutMapping("/number/{id}")
    public String putPetNumber(@PathVariable long id, @RequestBody PetChangeRequest request) {
        petService.putPet(id, request);

        return "OK";
    }
}
